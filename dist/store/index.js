"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _configureStore.default;
  }
});

var _configureStore = _interopRequireDefault(require("./configureStore"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }