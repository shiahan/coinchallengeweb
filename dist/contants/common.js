"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.supportLang = exports.breakPoint = void 0;
var breakPoint = '800px';
exports.breakPoint = breakPoint;
var supportLang = ['en_US', 'zh_TW', 'zh_CN'];
exports.supportLang = supportLang;