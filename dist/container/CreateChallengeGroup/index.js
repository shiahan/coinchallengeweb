"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _CreateChallengeGroup.default;
  }
});

var _CreateChallengeGroup = _interopRequireDefault(require("./CreateChallengeGroup"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }