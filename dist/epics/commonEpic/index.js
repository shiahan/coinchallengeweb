"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _commonEpic.default;
  }
});

var _commonEpic = _interopRequireDefault(require("./commonEpic"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }