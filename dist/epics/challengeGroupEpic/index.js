"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _challengeGroupEpic.default;
  }
});

var _challengeGroupEpic = _interopRequireDefault(require("./challengeGroupEpic"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }