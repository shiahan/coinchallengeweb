import React from 'react'
import { RouteComponentProps } from 'react-router'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'
import ChallengeCard from './components/ChallengeCard'
import ChallengeInfo from './components/ChallengeInfo'
import SponsorButton from './components/SponsorButton'
import Sponsers from './components/Sponsers'
import { ChallengeStateType } from '@Reducers/challengeReducer'
import { CommonStateType } from '@Reducers/commonReducer'
import HistoryTimeline from './components/HistoryTimeline'
import Notifier from './components/Notifier'
import { breakPoint, hostUrl } from '@Src/contants/common'

import Contract from 'web3/eth/contract'
import { setPopup, SetPopProps, initContract } from '@Epics/commonEpic/action'
import {
  getChallenge,
  sponserChallenge,
  SponserProp
} from '@Epics/challengeEpic/action'
import {
  ChallengeType,
  Sponsor,
  ChainType,
  RouteParams
} from '@Src/typing/globalTypes'
import { isDexon } from '@Src/utils'

import { injectIntl, InjectedIntlProps } from 'react-intl'
import Transaction from '@Components/Transaction'
import { changeRoute } from '@Utils/index'
import web3 from 'web3'

import { getLang } from '@Src/translation'
import {
  sponsorEvents,
  getPastSponsor,
  canSponsor
} from '@Src/contracts/contractService'

import { APP_COIN } from '@Src/contants/common'

const ChallengeContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  marginTop: '30px',
  [`@media (max-width: ${breakPoint})`]: {
    width: '100%',
    overflow: 'hidden',
    marginTop: 0
  }
})

const StyledGridList = styled('div')({
  width: 800,
  zIndex: 1,
  [`@media (max-width: ${breakPoint})`]: {
    width: '100%'
  }
})

const LoadingBlk = styled('div')({
  maxWidth: '400px',
  margin: '0 auto',
  textAlign: 'center',
  a: {
    display: 'inline-block',
    marginBottom: '10px'
  }
})

interface ChallengeProp
  extends RouteComponentProps,
    ChallengeType,
    InjectedIntlProps {
  txContract: Contract | null
  contract: Contract | null
  account: string | null
  error: boolean
  isCofirmingSponsor: boolean
  txhash: string
  groupName: string
  groupImage: string
  minAmount: number
  startTimestamp: number
  fetchChallenge: (data: RouteParams) => void
  sponserChallenge: (payload: SponserProp) => void
  setChallengeSponsersAction: (sponsors: Sponsor[]) => void
  setPopup: (payload: SetPopProps) => void
  initContract: (chain?: string) => void
}

interface ChallengeState {
  sponsors: Sponsor[]
  sponsorAmount: number
  invalidAddress: boolean
}

const deeplinking = (data: RouteParams) => {
  branch.deepview(
    {
      channel: 'safari',
      feature: 'deepview',
      $uri_redirect_mode: 2,
      data: {
        $deeplink_path: `challenge/${data.chain}/${data.groupId}/${
          data.address
        }${data.round ? `/${data.round}` : ''}`,
        user_cookie_id: 'coin-challenge',
        ...data,
        blockchain: data.chain
      }
    },
    {
      open_app: true
    }
  )
}

const mapStateToProps = (state: Map<string, object>) => {
  const challengeState = state.get('challenge') as ChallengeStateType
  const commonState = state.get('common') as CommonStateType
  return {
    txContract: commonState.get('txContract'),
    contract: commonState.get('contract'),
    account: commonState.get('userAddress'),
    ...challengeState.toJS()
  }
}

const mapDispathToProps = (dispatch: Dispatch) => ({
  fetchChallenge: (data: RouteParams) =>
    dispatch(
      getChallenge({
        groupId: data.groupId,
        challenger: data.address,
        round: data.round
      })
    ),
  sponserChallenge: (payload: SponserProp) =>
    dispatch(
      sponserChallenge({
        ...payload,
        dispatch
      })
    ),
  setPopup: (payload: SetPopProps) => dispatch(setPopup(payload)),
  initContract: (chain?: string) => dispatch(initContract(chain))
})
class Challenge extends React.Component<ChallengeProp, ChallengeState> {
  public address: string = ''
  public groupId: string = ''
  public round: number | undefined = undefined
  public fetched: boolean = false
  public sponsorFetched: boolean = false
  public coin: string = 'ETH'
  public chain: ChainType = 'ethereum'

  constructor(props: ChallengeProp) {
    super(props)
    const params = this.props.match.params as RouteParams
    this.address = params.address
    this.groupId = params.groupId
    this.chain = params.chain
    this.coin = APP_COIN(this.chain)
    this.round = params.round ? Number(params.round) : undefined
    this.state = {
      sponsors: [],
      sponsorAmount: 0,
      invalidAddress: false
    }
  }

  private onNewSponsor = (sponsor: Sponsor) => {
    const sponsors = this.state.sponsors
    this.setState({
      sponsors: sponsors.concat([sponsor]),
      sponsorAmount:
        this.state.sponsorAmount + Number(web3.utils.fromWei(sponsor.amount))
    })
  }

  private async checkAndFetch() {
    const {
      contract,
      fetchChallenge,
      sponserSize,
      targetDays,
      setPopup,
      round
    } = this.props
    const isValid = await web3.utils.isAddress(this.address)
    if (!isValid && !this.fetched) {
      setPopup({
        showPop: true,
        messageKey: 'invalidAddress'
      })
      this.setState({
        invalidAddress: true
      })
      this.fetched = true
      return
    }

    if (typeof window !== 'undefined' && isValid) {
      deeplinking({
        chain: this.chain,
        address: this.address,
        groupId: this.groupId,
        round: this.round
      })
    }

    if (contract) {
      if (!this.fetched) {
        fetchChallenge({
          chain: this.chain,
          address: this.address,
          groupId: this.groupId,
          round: this.round
        })
        this.fetched = true
      } else if (!this.sponsorFetched && targetDays > 0) {
        this.sponsorFetched = true
        const sponsorData = await getPastSponsor(
          contract,
          round,
          this.groupId,
          this.address,
          sponserSize
        )
        sponsorEvents({
          contract,
          groupId: this.groupId,
          challenger: this.address,
          callback: this.onNewSponsor
        })

        this.setState({
          sponsors: sponsorData.data,
          sponsorAmount: sponsorData.data.reduce((accumulator, sponsor) => {
            return accumulator + Number(web3.utils.fromWei(sponsor.amount))
          }, 0)
        })
      }
    }
  }

  private checkWallet = () => {
    const { setPopup, txContract, intl } = this.props
    if (!this.props.txContract) {
      setPopup({
        showPop: true,
        popMessage: intl.formatMessage({
          id: isDexon(this.chain) ? 'dexonProviderNotFound' : 'providerNotFound'
        })
      })
      return false
    }
    return true
  }

  private onSponsor = async ({
    amount,
    comment
  }: {
    amount: number
    comment: string
  }) => {
    const { setPopup, minAmount, intl } = this.props
    if (amount < minAmount) {
      setPopup({
        showPop: true,
        popMessage: intl.formatMessage(
          { id: 'min.amount.error' },
          {
            amount: minAmount + ' ' + this.coin
          }
        )
      })
    } else {
      const { txContract, account, sponserChallenge } = this.props
      if (txContract && account) {
        sponserChallenge({
          groupId: this.groupId,
          who: this.address,
          amount,
          comment
        })
      }
    }
  }

  private canSponsor = async () => {
    let res = false
    if (this.props.contract) {
      res = await canSponsor(this.props.contract, this.groupId, this.address)
    }

    return res
  }

  public componentDidUpdate() {
    this.checkAndFetch()
  }

  public componentDidMount() {
    const { history, location, initContract } = this.props
    changeRoute({ history, location, match: {} })
    initContract(this.chain)
    this.checkAndFetch()
  }

  public render() {
    const {
      completeDays,
      totalDays,
      targetDays,
      amount,
      goal,
      intl,
      isCofirmingSponsor,
      txhash,
      contract,
      groupName,
      groupImage
    } = this.props

    intl.locale

    const goalText = intl.formatMessage(
      { id: `group.unit.${this.groupId}`, defaultMessage: ' ' },
      { goal }
    )

    const title =
      intl.formatMessage({
        id: `group.title.${this.groupId}`,
        defaultMessage: 'CoinChallenges'
      }) +
      ' - ' +
      goalText

    const shareDesc = intl.formatMessage(
      {
        id: 'shareDesc'
      },
      { amount: `${amount} ${this.coin}`, totalDays }
    )

    return (
      <React.Fragment>
        <ChallengeContainer>
          <Helmet>
            <title>{title}</title>
            <meta property='og:title' content={title} />
            <meta property='og:description' content={shareDesc} />
            <meta
              property='og:image'
              content={`${hostUrl()}share/${this.chain}/${this.groupId}/${
                this.address
              }?l=${getLang()}`}
            />
            <meta property='og:site_name' content='CoinChallengs' />
            <meta property='twitter:card' content='summary_large_image' />
            <meta property='twitter:site' content='CoinChallengs' />
            <meta property='twitter:creator' content='CoinChallengs' />
            <meta property='twitter:image:alt' content={shareDesc} />
          </Helmet>
          <StyledGridList>
            <ChallengeCard
              name={groupName}
              goal={totalDays ? goalText : ''}
              url={groupImage}
              invalidAddress={this.state.invalidAddress}
            />
            <ChallengeInfo
              address={this.address}
              completeDays={completeDays}
              targetDays={targetDays}
              totalDays={totalDays}
              amount={amount}
              invalidAddress={this.state.invalidAddress}
              coin={this.coin}
            />
            {totalDays && this.canSponsor() ? (
              <SponsorButton
                chain={this.chain}
                onSponsor={this.onSponsor}
                checkWallet={this.checkWallet}
                intl={intl}
              />
            ) : null}
            {isCofirmingSponsor ? (
              <LoadingBlk>
                <Transaction txHash={txhash} />
              </LoadingBlk>
            ) : null}
            <Sponsers sponsors={this.state.sponsors} coin={this.coin} />
            <HistoryTimeline
              contract={contract}
              groupId={this.groupId}
              challenger={this.address}
              coin={this.coin}
            />
          </StyledGridList>
        </ChallengeContainer>
        <Notifier contract={contract} coin={this.coin} />
      </React.Fragment>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispathToProps
)(injectIntl(Challenge))
